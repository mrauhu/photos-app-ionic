angular.module('starter.services', ['ngResource'])

  .factory('Student', function($resource, api){
    console.log('Student factory');
    return {
      list: $resource( api + '/student/').query
    }
  })

;
