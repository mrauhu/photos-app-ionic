angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope, $state, Student, api, $cordovaCamera, $ionicPopup, $timeout, $http, $cordovaFileTransfer) {

    /**
     * Get student list
     */
    $scope.students = Student.list();

    /**
     * Take photo
     * @param student
     */
    $scope.getPhoto = function( student ) {

      /**
       * Options for camera
       * @type Object
       */
      var options = {
        quality : 75,
        destinationType : Camera.DestinationType.DATA_URL,
        sourceType : Camera.PictureSourceType.CAMERA,
        allowEdit : true,
        encodingType: Camera.EncodingType.JPEG,
        targetWidth: 300,
        targetHeight: 300,
        popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: false
      };

      // Make photo
      $cordovaCamera.getPicture(options)
        .then(function(imageData) {
          $scope.imgUri = "data:image/jpeg;base64," + imageData;
          $scope.student = student;

          // An elaborate, custom popup
          var myPopup = $ionicPopup.show({
            template: ' <img style="width: 171px; height: 128px;" ng-src="{{imgUri}}">',
            title: 'Student: ' + student.name,
            subTitle: 'Upload image or retake photo',
            scope: $scope,
            buttons: [
              { // Array[Object] (optional). Buttons to place in the popup footer.
                text: 'Retake',
                type: 'button-default',
                onTap: function(e) {
                  // e.preventDefault() will stop the popup from closing when tapped.
                  e.preventDefault();
                  $scope.getPhoto( student );
                  myPopup.close();
                  return true;
                }
              },
              {
                text: '<b>Upload</b>',
                type: 'button-positive',
                onTap: function(e) {
                  e.preventDefault();
                  $scope.uploadPhoto( student.id );
                  myPopup.close();
                  return true;
                }
              }
            ]
          });
          myPopup.then(function(res) {
            console.log('Tapped!', res);
            console.log( res );
          });
          //$timeout(function() {
          //  myPopup.close(); //close the popup after 3 seconds for some reason
          //}, 10000);

      }, function(err) {
        // An error occurred. Show a message to the user
        console.log('Error get picture', err);
      });
    };

    /**
     * Upload photo
     * @param student_id
     */
    $scope.uploadPhoto = function( student_id ) {
      /**
       * Options for file transfer
       * @type Object
       */
      var options = {
        fileKey: "file",
        fileName: "image.jpg",
        chunkedMode: false,
        mimeType: "image/jpeg"
      };

      // Send file
      $cordovaFileTransfer
        .upload(
          api + '/file/upload/' + student_id,
          $scope.imgUri,
          options
        )
        .then(function(result){
          console.log('Upload image', result);
          //console.log( JSON.stringify( result ));
        }, function(error){
          console.error('Error uploading image', error);
          //console.error( JSON.stringify( error ));
        }, function(progress){
          //console.log( JSON.stringify( progress ) );
        });
    }

  })

;
